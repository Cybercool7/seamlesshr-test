<?php

namespace App\Http\Controllers;

use App\Course;
use App\Enrolment;
use Illuminate\Http\Request;

class UserCourseController extends Controller
{
    private $arr = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $courses = Course::all();

        foreach ($courses as $c) {
            $e = Enrolment::where(['course_id'=>$c->id,'user_id'=>$id])->count();

            if($e){
                $co = Course::where('id',$c->id)->first();
                $en = Enrolment::where(['course_id'=>$c->id,'user_id'=>$id])->first();

                $a = ['course_name'=>$co->name,'date_enrolled'=>$en->created_at];
                array_push($this->arr, $a);
            }
            else{
                $co = Course::where('id',$c->id)->first();
                $a = ['course_name'=>$co->name,'date_enrolled'=>null];
                array_push($this->arr, $a);
            }
        }

        return response()->json($this->arr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
