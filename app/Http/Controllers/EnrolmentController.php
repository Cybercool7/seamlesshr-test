<?php

namespace App\Http\Controllers;

use App\Enrolment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnrolmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $Enrolmen Enrolment::comments;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('course_list')){
            for ($i = 0; $i < size($request->course_list) ; $i++) { 
                Enrolment::create([
                    'course_id'=>$request->course_list[$i],
                    'user_id'=>$request->user_id,
                ]);
            }

            return response()->json(['message'=>'Course Enrolment Successful!']);
        }
        else{ return response()->json(['error'=>'Select atleast a course'],401); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $courseList = $this->index();

        foreach ($courseList as $cl) {
            
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
